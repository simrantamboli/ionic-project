import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginpopupComponent } from './loginpopup.component';


const routes: Routes = [
  {
    path: '',
    component: LoginpopupComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class loginpopupRoutingModule {}