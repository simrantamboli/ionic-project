import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup ,Validators} from '@angular/forms';
import { ModalController, NavController  } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
form :FormGroup;
  
  constructor(public modalCtrl: ModalController,
    public navCtrl: NavController, public formBuilder: FormBuilder,
   ) {
      this.form = this.formBuilder.group({
    email: new FormControl(
      '',
      Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])
    ),

  });
    }
      
    async login() {
      if (this.form.valid) {
        console.log('form submitted')
      }
    }  
     

  ngOnInit() {}
  
  
  dismiss() {
    this.modalCtrl.dismiss();
  }
}
